#!/usr/bin/env python 

def add_numbers(a: int, b: int) -> (int, int, int):
   return a, b, a + b



def add_two_numbers_with_default(a: int, b: int = 3):
   return a + b


def my_function(*dogs):
   for dog in dogs:
      print(dog)

my_function("spot", "jim", "cating")