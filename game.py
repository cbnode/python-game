import random
from colorama import Fore, init
from classes import Player, Room, Game
import armory

def welcome():
    print("")
    print(Fore.RED + "                                         D U N G E O N")
    print(Fore.GREEN + """   
    The village of Honeywood has been terrorized by strange, deadly creatures for months now. Unable to endure any 
    longer, the villagers pooled their wealth and hired the most skilled adventurer they could find: you. After
    listening to their tale of woe, you agree to enter the labyrinth where most of the creatures seem to originate,
    and destroy the foul beasts. Armed with a longsword and a bundle of torches, you descend into the labyrinth, 
    ready to do battle....
   
    """)


def play_game():
    # init ensure that colorama works on different platforams
    init()
    adventurer = Player() 
    current_game = Game(adventurer)

    welcome()
    #get player input
    input("Press ENTER to continue")
    explore_labyrinth(current_game)


# Generate room
def generate_room() -> Room:
  items = []

  # There is a 25% chance that you will get an item in the room 
  if random.randint(1, 100) < 26:
      i = random.choice(list(armory.items.values())) 
      items.append(i)

  return Room(items) 


def explore_labyrinth(current_game: Game):
    while True:
        room = generate_room()
        current_game.room = room

        current_game.room.print_description() 

        for i in current_game.room.items:
            print(f"{Fore.YELLOW}You see a {i['name']}")

        player_input = input(Fore.YELLOW + "->").lower().strip()
        #print(player_input)

        # Do something with the input. 
        if player_input == "help":
            show_help()

        elif player_input in ["n", "s", "e", "w"]:
            print(f"{Fore.MAGENTA}You move deeper into the Dungeon!")
            continue
        
        elif player_input == "quit":
            print(" Overcome with terror, you flee the Dungeonn, and are branded a cowerd")
            # TODO Print out final score.
            play_again()
            
        
        else:
            print("I'm not to sure what you mean... Type help for Help")

def play_again():
    yn = get_yn(Fore.BLUE + "Do you want to play again? ")
    if yn == "yes":
        play_game()
    else:
        print("Until next time, adventurer.")
        print("")
        exit(0)
      

def get_yn(question: str) -> str:
    while True:
        answer = input(question + "(yes/no) -> ").lower().strip()
        if answer not in ["yes", "no", "y", "no"]:
            print("Please enter yes or no.")
        else:
            if answer == "y":
                answer == "yes"
            elif answer == "n":
                answer == "no"
            return answer


def get_input():
    pass

def show_help():
    print(Fore.GREEN + """
    ENTER A COMMAND 
    - n/s/e/w: Move in a direction.
    - map - show a map of the labyrinth.
    - look - look around and describe your environment.
    - equip <item> - start using an item from you inventory.
    - unequip <item> - Stop using an item from your inventory
    - fight - Attack a foe
    - examine <object> - Examine an object. 
    - get <item> - Get Item
    - drop <item> Drop an item
    - rest - Restore some health by resting. 
    - inventory - show your inventory. 
    - status - Show current player status
    - quit - End the game. 
    """)
